#Artificial and Machine Intelligence

Curtin University - 2017

Implementation of beam informed search and limited memory A\* search between
two user specified nodes in an undirected graph.

###Usage

####Memory limited A* search.  
Navigate to the source directory and compile the program.  
`javac ALimSearchMain`

Run with the provided script.  
`alim-search <graph-edges_file> <heuristics_file> <start-node> <goal-node>`


####Beam Search
Navigate to the source directory and compile the program.  
`javac BeamSearchMain`

Run with the provided script.  
`beam-search <graph-edges_file> <heuristics_file> <beam-width> <start-node> <goal-node>`
