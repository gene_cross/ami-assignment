package beamSearch;

import java.util.HashMap;

public class Node implements Comparable<Node> {

    private final int ID;
    private double heuristic;
    private HashMap<Integer, Integer> edges;    // edges<nodeID, edgeWeight>
    private int parentID;

    @Override
    public boolean equals(Object obj) {
        return obj.getClass() == Node.class && this.ID == ((Node) obj).ID;
    }

    @Override
    public int compareTo(Node rhs) {
        double temp = this.heuristic - rhs.heuristic;
        if (temp < 0.0) {
            return -1;
        } else if (temp > 0.0) {
            return 1;
        } else {
            return 0;
        }
    }

    Node(int ID) {
        this.ID = ID;
        this.edges = new HashMap<>();
        this.parentID = -1;
    }

    int getID() {
        return ID;
    }

    void setHeuristic(double heuristic) {
        this.heuristic = heuristic;
    }

    HashMap<Integer, Integer> getEdges() {
        return edges;
    }

    // adds an edge between this node and another,
    // updates the edgeWeight if the edge already exists
    void addEdge(int vertex, int edgeWeight) {
        if (vertex == ID) {
            throw new IllegalArgumentException("Node cannot add an edge to itself.");
        }
        edges.put(vertex, edgeWeight);
    }

    int getParentID() {
        return parentID;
    }

    void setParentID(int parentID) {
        this.parentID = parentID;
    }

}
