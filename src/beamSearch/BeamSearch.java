package beamSearch;

import java.util.*;


public class BeamSearch {

    private LinkedList<Node> openList;      // nodes to be expanded
    private HashSet<Integer> closedList;    // evaluated nodes
    private LinkedList<LinkedList<Integer>> solutions;
    private LinkedList<LinkedList<Integer>> storedPaths;

    private Graph graph;
    private Node goalNode;
    private int beamWidth;

    public BeamSearch(Graph graph, int startNodeID, int goalNodeID, int beamWidth) {
        openList = new LinkedList<>();
        closedList = new HashSet<>();
        solutions = new LinkedList<>();
        storedPaths = new LinkedList<>();

        this.graph = graph;
        Node startNode = graph.getNodeMap().get(startNodeID);
        this.goalNode = graph.getNodeMap().get(goalNodeID);
        this.beamWidth = beamWidth;

        if (startNode != null && goalNode != null && startNode != goalNode && beamWidth > 0) {
            openList.add(startNode);
        } else {
            throw new IllegalArgumentException("Invalid BeamSearch() parameter/s.");
        }
    }

    public void search() {
        HashSet<Integer> successors = new HashSet<>();

        // when continuing a search, do not want old stored paths
        storedPaths.clear();

        // while there are nodes to be evaluated
        while (openList.size() != 0) {
            // generate successors
            successors.clear();
            for (Node node : openList) {
                for (Integer edge : node.getEdges().keySet()) {
                    // if the node has not already been evaluated
                    // and not already a successor
                    if (!closedList.contains(edge) && !successors.contains(edge)) {
                        graph.getNodeMap().get(edge).setParentID(node.getID());
                        successors.add(edge);
                    }
                }
                // node has been expanded and may be part of the beam
                // ultimately it will be evaluated
                closedList.add(node.getID());
            }
            openList.clear(); // all nodes in this list have been expanded

            // narrow the search to the beam width
            HashSet<Integer> beamNodes = narrowWidth(successors, beamWidth);

            // add beamNodes to the openList to be expanded later
            for (Integer nodeID : beamNodes) {
                openList.addLast(graph.getNodeMap().get(nodeID));
            }

            // for each node in the beam
            for (Integer nodeID : beamNodes) {
                // goal test
                if (nodeID == goalNode.getID()) {
                    // build the solutions path
                    solutions.addLast(buildPath(nodeID));

                    // build alternate paths, from open list
                    for (Node node : openList) {
                        storedPaths.add(buildPath(node.getID()));
                    }

                    // remove this node from the openList to allow the search to continue properly,
                    // without looping on this node
                    openList.remove(graph.getNodeMap().get(nodeID));

                    // finished with this search
                    return;
                }
            }
        }
    }


    // builds and returns a path stored in a new linkedList,
    // the path will be from the startNode to the given node
    private LinkedList<Integer> buildPath(int nodeID) {
        LinkedList<Integer> store = new LinkedList<>();
        store.addFirst(nodeID);
        Node temp = graph.getNodeMap().get(nodeID);
        // follow the parents from the given node to the start node
        while (temp.getParentID() != -1) {
            store.addFirst(temp.getParentID());
            temp = graph.getNodeMap().get(temp.getParentID());
        }
        return store;
    }

    public LinkedList<LinkedList<Integer>> getSolutions() {
        return solutions;
    }

    public LinkedList<LinkedList<Integer>> getStoredPaths() {
        return storedPaths;
    }

    // takes the Set, sorts it, and returns a new set of size K (beamWidth)
    private HashSet<Integer> narrowWidth(Set<Integer> nodeIDSet, int beamWidth) {
        List<Node> nodeList = new ArrayList<>();

        // get the nodes associated with the given ID's
        for (Integer ii : nodeIDSet) {
            // if not already evaluated
            if (!closedList.contains(ii) && !openList.contains(graph.getNodeMap().get(ii))) {
                nodeList.add(graph.getNodeMap().get(ii));
            }
        }

        Collections.sort(nodeList);

        if (nodeList.size() < beamWidth) {
            beamWidth = nodeList.size();
        }

        HashSet<Integer> bestSet = new HashSet<>();
        for (int ii = 0; ii < beamWidth; ii++) {
            bestSet.add(nodeList.get(ii).getID());
        }
        return bestSet;
    }
}

