package beamSearch;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Scanner;

public class Graph {

    private HashMap<Integer, Node> nodeMap;

    public Graph() {
        nodeMap = new HashMap<>();
    }

    HashMap<Integer, Node> getNodeMap() {
        return nodeMap;
    }

    public void loadEdges(String filename) throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader(filename));
        while (scanner.hasNextInt()) {
            addEdge(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
        }
    }

    // adds an edge between vertices, one and two
    private void addEdge(int one, int two, int edgeWeight) {
        if (one == two) {
            throw new IllegalArgumentException("Vertices cannot be the same.");
        }
        if (!nodeMap.containsKey(one)) {
            Node node = new Node(one);
            node.addEdge(two, edgeWeight);
            nodeMap.put(one, node);
        } else {
            nodeMap.get(one).addEdge(two, edgeWeight);
        }

        if (!nodeMap.containsKey(two)) {
            Node node = new Node(two);
            node.addEdge(one, edgeWeight);
            nodeMap.put(two, node);
        } else {
            nodeMap.get(two).addEdge(one, edgeWeight);
        }
    }

    public void loadHeuristics(String filename) throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader(filename));
        while (scanner.hasNextInt()) {
            Node node = nodeMap.get(scanner.nextInt());
            if (node != null) {
                node.setHeuristic(scanner.nextDouble());
            }
        }
    }
}
