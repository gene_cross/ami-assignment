import beamSearch.BeamSearch;
import beamSearch.Graph;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class BeamSearchMain {

    public static void main(String[] args) {
        Graph graph = new Graph();
        Scanner scanner = new Scanner(System.in);

        if (args.length == 5) {
            try {
                graph.loadEdges(args[0]);
                graph.loadHeuristics(args[1]);

                BeamSearch beamSearch = new BeamSearch(graph, Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[2]));

                while (true) {
                    beamSearch.search();

                    System.out.print("\nSolutions: ");
                    printPaths(beamSearch.getSolutions());

                    System.out.print("\nStored Paths:");
                    printPaths(beamSearch.getStoredPaths());

                    // continue search for more solutions
                    System.out.print("\nContinue search? (Y|N): ");
                    String response = scanner.next();
                    if (response.equals("Y") || response.equals("y")) {
                        // continue;
                    } else {
                        break;
                    }
                }

            } catch (FileNotFoundException e) {
                System.out.println("Could not find file/s: \"" + args[0] + "\" and/or \"" + args[1] + "\"");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("Usage: java BeamSearchMain <graph-edges file> <heuristics file> <BeamWidth> <start-node> <goal-node>");
        }
    }

    public static void printPaths(LinkedList<LinkedList<Integer>> paths) {
        for (LinkedList<Integer> path : paths) {
            System.out.println();
            for (Integer integer : path) {
                System.out.print(integer + " ");
            }
        }
    }
}
