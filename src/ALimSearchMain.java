import memoryLimitedAStarSearch.ALimSearch;
import memoryLimitedAStarSearch.Graph;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class ALimSearchMain {

    public static void main(String[] args) {
        Graph graph = new Graph();
        Scanner scanner = new Scanner(System.in);

        if (args.length == 4) {
            try {
                graph.loadEdges(args[0]);
                graph.loadHeuristics(args[1]);

                ALimSearch aLimSearch = new ALimSearch(graph, Integer.parseInt(args[2]), Integer.parseInt(args[3]));

                while (true) {
                    if (aLimSearch.search()) {

                        System.out.print("\nSolutions: ");
                        printPaths(aLimSearch.getSolutions());

                        System.out.print("\nStored Paths:");
                        printPaths(aLimSearch.getStoredPaths());

                        // continue search for more solutions
                        System.out.print("\nContinue search? (Y|N): ");
                        String response = scanner.next();
                        if (response.equals("Y") || response.equals("y")) {
                            continue;
                        } else {
                            break;
                        }
                    }
                    else {
                        System.out.println("No solution could be found;");
                        break;
                    }
                }

            } catch (FileNotFoundException e) {
                System.out.println("Could not find file/s: \"" + args[0] + "\" and/or \"" + args[1] + "\"");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("Usage: java ALimSearchMain <graph-edges file> <heuristics file> <start-node> <goal-node>");
        }
    }

    private static void printPaths(LinkedList<LinkedList<Integer>> paths) {
        for (LinkedList<Integer> path : paths) {
            System.out.println();
            for (Integer integer : path) {
                System.out.print(integer + " ");
            }
        }
    }

}
