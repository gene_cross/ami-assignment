package memoryLimitedAStarSearch;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;

public class ALimSearch {

    private LinkedList<Node> openList;      // nodes to be expanded
    private HashSet<Integer> closedList;    // evaluated nodes
    private LinkedList<LinkedList<Integer>> solutions;
    private LinkedList<LinkedList<Integer>> storedPaths;

    private Graph graph;
    private Node goalNode;

    // for comparison when continuing search, to see if the new path is also optimal
    private int bestFscore;


    public ALimSearch(Graph graph, int startNodeID, int goalNodeID) {
        openList = new LinkedList<>();
        closedList = new HashSet<>();
        solutions = new LinkedList<>();
        storedPaths = new LinkedList<>();

        this.graph = graph;
        Node startNode = graph.getNodeMap().get(startNodeID);
        goalNode = graph.getNodeMap().get(goalNodeID);
        bestFscore = 0;

        if (startNode != null && goalNode != null && startNode != goalNode) {
            openList.add(startNode);
        } else {
            throw new IllegalArgumentException("Invalid ALimSearch() parameter/s.");
        }
    }

    // returns true if a path is found, else false
    public boolean search() {
        // when continuing a search, do not want old stored paths
        storedPaths.clear();

        while (!openList.isEmpty()) {

            // get next best node and remove it from the openList
            Node currentNode = openList.pop();

            // goal test
            if (currentNode.getID() == goalNode.getID()) {
                // if new path is the first solution
                // or if new path is an alternate optimal solution
                if (solutions.isEmpty() || (currentNode.getFscore() == bestFscore)) {
                    // build the solution path
                    solutions.addLast(buildPath(currentNode.getID()));
                    // build 'stored' paths
                    for (Node node : openList) {
                        storedPaths.addLast(buildPath(node.getID()));
                    }
                    // set path cost for comparison with future paths
                    bestFscore = currentNode.getFscore();
                    return true;
                }
                else {
                    // path is sub optimal
                    return false;
                }
            }
            // currentNode has been evaluated
            closedList.add(currentNode.getID());

            // get successors and add them to the openList
            for (Integer edge : currentNode.getEdges().keySet()) {
                Node successor = graph.getNodeMap().get(edge);

                // if node is already evaluated go to next node
                if (closedList.contains(successor.getID())) {
                    continue;
                }

                // if the successor is already on the openList
                if (openList.contains(successor)) {
                    // update it if the new path to it is better
                    Node existingNode = openList.get(openList.indexOf(successor));
                    if (successor.getFscore() < existingNode.getFscore()) {
                        existingNode.update(currentNode.getID(), graph);
                    }
                }
                else {
                    successor.update(currentNode.getID(), graph);
                    openList.addLast(successor);
                }
            }
            Collections.sort(openList);
        }

        // goal node could not be found
        return false;
    }

    // builds and returns a path stored in a new linkedList,
    // the path will be from the startNode to the given node
    private LinkedList<Integer> buildPath(int nodeID) {
        LinkedList<Integer> store = new LinkedList<>();
        store.addFirst(nodeID);
        Node temp = graph.getNodeMap().get(nodeID);

        // follow the parents from the given node to the start node
        while (temp.getParentID() != -1) {
            store.addFirst(temp.getParentID());
            temp = graph.getNodeMap().get(temp.getParentID());
        }
        return store;
    }

    public LinkedList<LinkedList<Integer>> getSolutions() {
        return solutions;
    }

    public LinkedList<LinkedList<Integer>> getStoredPaths() {
        return storedPaths;
    }

}





















